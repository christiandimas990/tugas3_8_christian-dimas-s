<?php
session_start();
if(isset($_SESSION['username'])) {
    header("location:home.php");
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Sign Up</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <style>
body {
  background-image: url('bg.jpg');
  background-repeat: no-repeat;
  background-attachment: fixed;
  background-size: cover;
}
form {
  background-color : #e6e6e6;
}
h2 {
  color : white;
  font-family : sans-serif;
}
h4 {
  color : red;
}
</style>
</head>
<body>

<div class="container">
  <h2 align='center'>SIGN UP</h2>
  <h4 align='center'>
  <?php 
	if(isset($_GET['pesan'])){
		if($_GET['pesan'] == "gagal"){
			echo "*Semua data harus diisi!";
		}
    if($_GET['pesan'] == "password_salah"){
			echo "*Password Konfirmasi dan Password asli tidak sama";
		}
	}
	?>
    </h4>
  <form action="" method="post">
  <div class="form-group row">
		<label for="name" class="col-sm-2 col-form-label">Nama:</label>
		<div class="col-sm-10">
			<input name="name" class="form-control" placeholder="Nama">
		</div>
	</div>
 
	<div class="form-group row">
		<label for="username" class="col-sm-2 col-form-label">Username</label>
		<div class="col-sm-10">
			<input name="username" class="form-control" placeholder="Username">
		</div>
	</div>
    
    <div class="form-group row">
		<label for="email" class="col-sm-2 col-form-label">Email:</label>
		<div class="col-sm-10">
			<input name="email" class="form-control" placeholder="Email">
		</div>
	</div>

    <div class="form-group row">
		<label for="password" class="col-sm-2 col-form-label">Password:</label>
		<div class="col-sm-10">
			<input name="password" type="password" class="form-control" placeholder="Password">
		</div>
	</div>

    <div class="form-group row">
		<label for="password" class="col-sm-2 col-form-label">Konfirmasi Password:</label>
		<div class="col-sm-10">
			<input name="confirm_password" type="password" class="form-control" placeholder="Konfirmasi Password">
		</div>
	</div>

    <div class="text-center"> 
            <input type="submit" class="btn btn-primary" name="simpan">
    </div> 

  </form>
</div>

</body>
</html>

<?php
include 'db.php';

if (@$_POST['simpan']) {

  $name = $_POST['name'];
  $username = $_POST['username'];
  $email = $_POST['email'];
  $password = $_POST['password'];
  $confirm_password = $_POST['confirm_password'];

  $data = $_POST;

  if (empty($data['name']) ||
    empty($data['username']) ||
    empty($data['password']) ||
    empty($data['email']) ||
    empty($data['confirm_password'])) {
    
    header("location:register.php?pesan=gagal");
}
  if ($confirm_password !== $password) {
    header("location:register.php?pesan=password_salah");
  }

  $hash_pass = password_hash($password, PASSWORD_DEFAULT);

  mysqli_query($conn, "INSERT INTO user (name,username,email,password) VALUES ('$name', '$username', '$email', '$hash_pass')");

?>

<script type="text/javascript">
  alert("SIMPAN berhasil");
  window.location.href="login.php"

</script>

<?php  
  }
 ?>