<?php
session_start();
if(!isset($_SESSION["username"])) {
    header("location:login.php");
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <style>
        a {
            color : white;
            text-decoration : none;
        }

        a:hover {
            color : white;
            text-decoration : underline;
        }

        tr{
            text-align:center;
        }
    </style>
    <title>Terra Photography</title>
</head>

<body>
    <div class="container p-2">

        <div class="row p-3 mt-4"> 
            <div class="col-md-3 col-sm-6 col-xs-6">
                <div class="text-center">
                    <a href="#">
                        <img src="https://images.pexels.com/photos/36717/amazing-animal-beautiful-beautifull.jpg?auto=compress&cs=tinysrgb&dpr=2&w=500" class="img-fluid img-thumbnail" alt="">
                    </a>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-6">
                <div class="text-center">
                    <a href="#">
                        <img src="https://images.pexels.com/photos/853199/pexels-photo-853199.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500" class="img-fluid img-thumbnail" alt="">
                    </a>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-6">
                <div class="text-center">
                    <a href="#">
                        <img src="https://images.pexels.com/photos/346529/pexels-photo-346529.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500" class="img-fluid img-thumbnail" alt="">
                    </a>
                </div>
            </div>

            <div class="col-md-3 col-sm-6 col-xs-6">
                <div class="text-center">
                    <a href="#">
                        <img src="https://images.pexels.com/photos/1261728/pexels-photo-1261728.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500" class="img-fluid img-thumbnail" alt="">
                    </a>
                </div>
            </div>

        </div>
        <div class="row p-3 mt-3"> 
                <h3>Selamat datang di Terra Photography, <?php echo $_SESSION['username']; ?></h3>
                

                    <table class="table table-hover table-dark">
                    <thead>
                        <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Username</th>
                        <th scope="col">Nama</th>
                        <th scope="col">Email</th>
                        <th colspan = "2" scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php  
                        include 'db.php';
                        $data = mysqli_query($conn ,"SELECT id, name, username, email FROM user");
                        foreach (mysqli_fetch_all($data, MYSQLI_ASSOC) as $value) {
                    ?>
                        <tr>
                        <td><?php echo $value['id']; ?></td>
                        <td><?php echo $value['name']; ?></td>
                        <td><?php echo $value['username']; ?></td>
                        <td><?php echo $value['email']; ?></td>
                        <td><a href="ubah.php"><?php $_SESSION['id'] = $value['id'];?>UBAH</td>
                        <td><a href="hapus_proses.php"><?php $_SESSION['id'] = $value['id'];?>HAPUS</a></td>
                        </tr>
                    <?php
                        }
                    ?>
                    </tbody>
                    </table>

                
                    
        </div>

        <div class="row border-top p-2">
            <div class="col text-center">
            <button type="button" class="btn btn-secondary"> <a href="logout.php">Logout</a> </button>
            </div>
            <div class="col text-center">
            <button type="button" class="btn btn-secondary"> <a href="tambah.php">Tambah</a> </button>
            </div>    
        </div>
        <div class="row border-top p-2">
            <div class="col text-center">
                Copyright © 2021 Christian Dimas S
            </div>  
        </div>
            
    </div>
</body>

</html>